var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var iteration = 11
var initialSize = 0.45
var dragonVertices
var step = 2
var growingState = true
var rotation = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  dragonVertices = calculateDragonVertices(initialSize, iteration, dragonCurve(iteration))
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < 4; j++) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    push()
    rotate(Math.PI * 0.5 * j + rotation * Math.PI * 0.5)
    for (var i = 0; i < Math.floor(step); i++) {
      if (dragonVertices[i + 1] !== undefined) {
        fill(64 + (255 - 64) * 0.25 * j)
        noStroke()
        rect(boardSize * (dragonVertices[i][0] + dragonVertices[i + 1][0]) * 0.5, boardSize * (dragonVertices[i][1] + dragonVertices[i + 1][1]) * 0.5, boardSize * dist(dragonVertices[i][0], dragonVertices[i][1], dragonVertices[i + 1][0], dragonVertices[i + 1][1]) * 0.5, boardSize * dist(dragonVertices[i][0], dragonVertices[i][1], dragonVertices[i + 1][0], dragonVertices[i + 1][1]) * 0.5)
      }
    }
    pop()
    pop()
  }

  if (growingState === true) {
    step += deltaTime * 0.25
  } else {
    step -= deltaTime * 0.25
  }
  if (step >= dragonVertices.length - 1) {
    growingState = false
  }
  if (step <= 2) {
    rotation--
    growingState = true
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function calculateDragonVertices(size, iteration, dragon) {
  var rotation = 0
  var position = [0, 0]
  var vertices = [
    [0, 0]
  ]
  for (var i = 0; i < dragon.length; i++) {
    var vert = [0, 0]
    if (dragon[i] === 'F') {
      position[0] += sin(rotation) * size / pow(sqrt(2), iteration)
      position[1] += cos(rotation) * size / pow(sqrt(2), iteration)
      vert = position
      vertices.push(vert.slice())
    } else if (dragon[i] === '+') {
      rotation += Math.PI * 0.5
    } else if (dragon[i] === '-') {
      rotation += Math.PI * 0.5 * (-1)
    }
  }
  return vertices
}

function dragonCurve(iteration) {
  var axiom = 'FX'
  var ruleX = 'X+YF+'
  var ruleY = '-FX-Y'
  var dragon = axiom.split('')
  for (var i = 0; i < iteration; i++) {
    for (var j = 0; j < dragon.length; j++) {
      if (dragon[j] === 'X') {
        dragon[j] = ruleX.split('')
      }
      if (dragon[j] === 'Y') {
        dragon[j] = ruleY.split('')
      }
    }
    dragon = dragon.flat()
  }
  return dragon
}
